TARGET=div-test
SRCS=$(wildcard *.cpp)
OBJS=$(SRCS:%.cpp=%.o)

CPPFLAGS+=-g

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

clean:
	rm -f $(TARGET) $(OBJS)

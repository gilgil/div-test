#include <stdio.h>
#include <stdlib.h>

void usage() {
	printf("syntax : div-test <num1> <num2>\n");
	printf("sample : div-test 10 5\n");
}

int main(int argc, char* argv[]) {
	if (argc != 3) {
		usage();
		return -1;
	}
	int num1 = atoi(argv[1]);
	int num2 = atoi(argv[2]);
	int q = num1 / num2;
	printf("%d / %d = %d\n", num1, num2, q);
}
